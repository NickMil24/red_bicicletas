const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const Usuario = require('../models/usuario')
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const FacebookStrategy = require('passport-facebook');

passport.use(new LocalStrategy(
    function (email, password, done){
      Usuario.findOne({email: email}, (err, usuario) => {
        if(err) return done(err)
        if(!usuario) return done(null, false, {message: `Email incorrecto`})
        if(!usuario.validPassword(password)) return done(null, false, {message : 'Password incorrecto'})
  
        return done(null, usuario)
      })
    }
))

passport.use(new GoogleStrategy ({
     clientID: process.env.GOOGLE_CLIENT_ID,
     clientSecret: process.env.GOOGLE_CLIENT_SECRET,
     callbackURL: process.env.HOST + "/auth/google/callback"
},
    function(accessToken, refreshToken, profile, cb) {
      console.log(profile);
      
      Usuario.findOneOrCreatedByGoogle(profile, function (err, user) {
         return cb(err, user);
      });
    })
);    

passport.use(new FacebookStrategy({
  clientID:  process.env.FACEBOOK_ID,
  clientSecret: process.env.FACEBOOK_SECRET,
  callbackURL: process.env.HOST + "auth/facebook/callback"
},
function(accessToken, refreshToken, profile, cb) {
  Usuario.findOneOrCreateByFacebook({ facebookId: profile.id }, function (err, user) {
    return cb(err, user);
  });
}
));


passport.serializeUser(function(usuario, done){
    done(null, usuario.id) // usa el id de mongo para hacer el matching entre la session y los datos del usuario
})
  
passport.deserializeUser(function(id, cb){
    Usuario.findById(id, function(err, usuario){
        cb(err, usuario)
    })
})

module.exports = passport