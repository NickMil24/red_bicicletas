var express = require('express');
var passport = require('passport');
var router = express.Router();
var auth = require('../../controllers/api/authController');

router.post('/authenticate', auth.authenticate);
router.post('/forgotPassword', auth.forgotPassword);
router.post('/facebook_token', passport.authenticate('facebook'), auth.authFacebookToken)
module.exports = router;