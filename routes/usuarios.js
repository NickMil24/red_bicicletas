var express = require('express');
var router = express.Router();
var usuariosController = require('../controllers/usuario')

/* GET users listing. */
router.get('/', usuariosController.list);
router.get('/create', usuariosController.createGet);
router.post('/create', usuariosController.create);
router.get('/:id/update', usuariosController.updateGet);
router.post('/:id/update', usuariosController.update);
router.post('/:id/delete', usuariosController.delete);

module.exports = router;