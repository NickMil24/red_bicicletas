var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
//[-25.31,-57.59]

describe('Testing Bicicletas', function(){
   
    beforeEach(function(done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.set('useCreateIndex', true)
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true});
       
        const db = mongoose.connection;
        db.on('error',console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('we are connected to test database!');
            done();
        });
   });

   afterEach(function(done) {
       Bicicleta.deleteMany({}, function(err, success){
           if (err) console.log(err);
           mongoose.disconnect(err);
           done();
       });
   });

   describe('Bicicleta.createInstance',() => {
       it('crea una instancia de Bicicleta', () => {
          var bici = Bicicleta.createInstance(1,"verde", "urbana", [-25.31,-57.59]);

          expect(bici.code).toBe(1);
          expect(bici.color).toBe("verde");
          expect(bici.modelo).toBe("urbana");
          expect(bici.ubicacion[0]).toEqual(-25.31);
          expect(bici.ubicacion[1]).toEqual(-57.59);
       })
   });

   describe('Bicicleta.allBicis', () => {
       it('comienza vacia', (done) => {
           Bicicleta.allBicis(function(err, bicis){
               expect(bicis.length).toBe(0);
               done();
           });
       });
   });

   describe('Bicicleta.add', () => {
       it('agrega solo una bici', (done) => {
           var aBici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
           Bicicleta.add(aBici, function(err, newBici){
               if (err) console.log(err);
               Bicicleta.allBicis(function(err, bicis){
                   expect(bicis.length).toEqual(1);
                   expect(bicis[0].code).toEqual(aBici.code);

                   done();
               });
           });
       });
   });

   describe('Bicicleta.findByCode', () =>{
       it('debe devolver la bici con code 1', (done) => {
           Bicicleta.allBicis(function(err,bicis){
               expect(bicis.length).toBe(0);

               var aBici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
               Bicicleta.add(aBici, function(err, newBici){
                   if (err) console.log(err);

                   var aBici2 = new Bicicleta({code: 2, color: "roja", modelo: "urbana"});
                   Bicicleta.add(aBici2, function(err, newBici){
                       if (err) console.log(err);
                       Bicicleta.findByCode(1, function (error, targetBici){
                           expect(targetBici.code).toBe(aBici.code);
                           expect(targetBici.color).toBe(aBici.color);
                           expect(targetBici.modelo).toBe(aBici.modelo);

                           done();
                       });
                   });
               });
           });
       });
   });

   describe('Bicicleta.deleteByCode',()=>{
        it('debe eliminar la bici con code 1', (done)=>{
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);
                var aBici1 = new Bicicleta({
                        code: 1,
                        color: "verde",
                        modelo: "urbana",
                        ubicacion: [-25.31,-57.59]
                    });
                Bicicleta.add(aBici1,function(err,newBici){
                    if (err) console.log(err);
                    
                    var aBici2 = new Bicicleta({
                        code: 2,
                        color: "roja",
                        modelo: "urbana",
                        ubicacion: [-25.31,-57.59]});
                    Bicicleta.add(aBici2,function(err,newBici){
                        if (err) console.log(err);
                        //console.log(newBici)
                        Bicicleta.removeByCode(1, function(err,rBici){
                            if (err) console.log(err);
                            Bicicleta.allBicis(function(err,bicis){
                                if (err) console.log(err);
                                expect(bicis.length).toBe(1);
                                done();
                            });
                        });
                    });
                }); 
            });   
        });
   });

   describe('Bicicleta.updateBycode',()=>{
        it('debe actualizar una bici', (done)=>{
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);
                var aBici1 = new Bicicleta({
                        code: 1,
                        color: "verde",
                        modelo: "ruta",
                        ubicacion: [-25.31,-57.59]
                    });
                Bicicleta.add(aBici1,function(err,newBici){
                    if (err) console.log(err);
                    var aBici2 = new Bicicleta({
                        code: 2,
                        color: "roja",
                        modelo: "urbana",
                        ubicacion: [-25.31,-57.59]});
                    Bicicleta.add(aBici2,function(err,newBici){
                        if (err) console.log(err);
                        var update = {color: "verde", modelo: "ruta", ubicacion: [-25.31,-57.59]};            
                        Bicicleta.updateByCode(2,update,function(err){
                                if (err) console.log(err);
                                Bicicleta.findByCode(2,function(err,targetBici){
                                    //console.log(targetBici)
                                    expect(targetBici.color).toBe(update.color);
                                    expect(targetBici.modelo).toBe(update.modelo);
                                    expect(targetBici.ubicacion[0]).toBe(update.ubicacion[0]);
                                    expect(targetBici.ubicacion[1]).toBe(update.ubicacion[1]);
                                    done();
                                });
                        });        
                    });
                });
            }); 
        });   
    });   

});

